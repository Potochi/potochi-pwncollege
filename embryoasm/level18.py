#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template /challenge/embryoasm_level1
from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('/challenge/embryoasm_level18')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR


def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        # return process(['python', '/challenge/debug.py', '18'], *a, **kw)
        return process([exe.path] + argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    Canary found
# NX:       NX enabled
# PIE:      PIE enabled

io = start()

print(io.recvuntil(b'bytes): \n').decode('utf-8'))

# x = rdi
# y = rax

# if [x] is 0x7f454c46:
#    y = [x+4] + [x+8] + [x+12]
# else if [x] is 0x00005A4D:
#    y = [x+4] - [x+8] - [x+12]
# else:
#    y = [x+4] * [x+8] * [x+12]

payload = b''
payload += asm(f'''
    mov eax, dword ptr [rdi + 4]
    cmp dword ptr [edi], 0x7f454c46
    jne elif

    add eax, dword ptr [rdi + 8]
    add eax, dword ptr [rdi + 12]

    jmp out
elif:
    cmp dword ptr [edi], 0x00005A4D
    jne else

    sub eax, dword ptr [rdi + 8]
    sub eax, dword ptr [rdi + 12]

    jmp out
else:
    xor edx, edx
    mov esi, dword ptr [rdi + 8]
    mul esi

    xor edx, edx
    mov esi, dword ptr [rdi + 12]
    mul esi
out:

''')

log.info(f'payload: {payload}')

io.send(payload)

io.interactive()
