#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template /challenge/embryoasm_level1
from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('/challenge/embryoasm_level22')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR


def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        #return process(['python', '/challenge/debug.py', '22'], *a, **kw)
        return process([exe.path] + argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    Canary found
# NX:       NX enabled
# PIE:      PIE enabled

io = start()

io.recvuntil(b'bytes): \n')

payload = b''
payload += asm('''
str_lower:
        xor rax, rax
        test rdi, rdi
        jz end
while_cond:
        movzx rsi, byte ptr [rdi]
        cmp rsi, 0
        je end
        cmp rsi, 90
        jg while_inc

        push rax
        push rdi
        mov rdi, rsi
        mov rbx, 0x403000
        call rbx
        pop rdi
        mov byte ptr [rdi], al
        pop rax
        inc rax
while_inc:
        inc rdi
        jmp while_cond
end:
    ret
''')    

log.info(f'payload: {payload}')

io.send(payload)

io.interactive()

