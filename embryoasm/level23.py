#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template /challenge/embryoasm_level1
from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('/challenge/embryoasm_level23')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR


def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        # return process(['python', '/challenge/debug.py', '23'], *a, **kw)
        return process([exe.path] + argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Full RELRO
# Stack:    Canary found
# NX:       NX enabled
# PIE:      PIE enabled

io = start()

io.recvuntil(b'bytes): \n')

# most_common_byte(src_addr, size):
#     b = 0
#     i = 0
#     for i <= size-1:
#         curr_byte = [src_addr + i]
#         [stack_base - curr_byte] += 1
#     b = 0

#     max_freq = 0
#     max_freq_byte = 0
#     for b <= 0xff:
#         if [stack_base - b] > max_freq:
#             max_freq = [stack_base - b]
#             max_freq_byte = b

#     return max_freq_byte


payload = b''
payload += asm('''
/* most_common_byte(rdi: src_addr, rsi: size) */

#define src_addr rdi
#define size rsi
#define counter rcx
#define vec_base rbx

#define max_freq rsp
#define max_byte rsp + 8

most_common_byte:
/* Function prologue */
    push rbp
    mov rbp, rsp

/* Make space for the frequency vector - uint64_t[256] */
    sub rsp, 256 * 8

/* Save a pointer to the first element of the array */
    mov vec_base, rsp

/* Local variables, uint64_t max_frequency, max_byte */
    push 0
    push 0

/*
    Zero out the memory inside the vector
*/
    mov counter, 256
_zero_loop:
    mov qword ptr [vec_base + counter * 8 - 8], 0
    loop _zero_loop

/* Count the frequency of the bytes */
    mov counter, size
_count_loop:
    movzx rax, byte ptr [src_addr + counter - 1]
    mov rdx, [vec_base + rax * 8]
    inc rdx
    mov [vec_base + rax * 8], rdx
    cmp rdx, [max_freq]
    jb _dont_set
    ja _set
    cmp rax, [max_byte]
    ja _dont_set
_set:
    mov [max_freq], rdx
    mov [max_byte], rax

_dont_set:
    loop _count_loop

    mov rax, [max_byte]
    add rsp, 256 * 8 + 16
    pop rbp
    ret
''')

log.info(f'payload: {payload}')

io.send(payload)

io.interactive()
